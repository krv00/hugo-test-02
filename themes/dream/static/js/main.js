"use strict";

$(document).ready(function() {
    $("body").overlayScrollbars({
        className: "os-theme-dark",
        nativeScrollbarsOverlaid: {
            initialize: !1
        },
        scrollbars: {
            autoHide: "scroll"
        }
    })
});