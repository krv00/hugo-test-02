"use strict";
$(document).ready(function() {
    initAccordion()
});

function randomInt(n, o) {
    return n = Math.ceil(n), o = Math.floor(o), Math.floor(Math.random() * (o - n)) + n
}

function initAccordion() {
    $(".dream-categories .ui.accordion").accordion({
        selector: {
            trigger: ".title .icon"
        }
    })
}